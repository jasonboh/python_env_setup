# Setting Up a Python Environment

## Overview

This document has all the steps needed to set up a Python environment. The instructions walk through the creation of a Python environment from scratch, without using [**Anaconda**](https://www.anaconda.com/). While Anaconda can certainly make certain setup tasks easy, there are some benefits to using just the standard Python tools instead. For example, Anaconda has recently started changing its licensing model, and it's possible it will not remain as easily accessible or free in the future. In contrast, the standard Python tools are always available and will always be free. Learning these ubiquitous tools and libraries will help you gain a better understanding of the standard Python ways of doing things. For example, this document makes use of **pip**, the standard Python package downloader/installer and **venv**, the standard library for creating and managing Python virtual environments. Anaconda has similar functionality, but it is accomplished in different ways, using Anaconda-specific tools and commands.

> **Note**: For this document, **Python 3.8** is used, but the instructions will work for newer versions of Python, with slight modifications.

## Step 1: Download and install Python

### Windows

If you are running Windows, download Python from <https://www.python.org>.  The default installation location for Python 3.8 is C:\\Users\\UserName\\AppData\\Local\\Programs\\Python\\Python38. This is fine, but it's also OK to change it.  During installation, you should make sure to install the `py launcher`.  There is no downside, and it provides some nice capabilties for picking which version of Python to launch when you have multiple Python versions.  You will also be asked if you’d like to add Python to your PATH.  If you install and use `py launcher` (as recommended), it is less important to add Python to your PATH.

### Linux

If you're running Linux, you likely already have Python installed.  If you want to use a different version of Python, you can do an "alt install" of another version.  Since there are many different Linux distributions, and the steps vary depending on the distribution, no step-by-step installation instructions are included here.

## Step 2: Running the Correct Python version

This section is especially important if you have more than one Python version installed.

### Windows

If you are on Windows, the version that gets launched if you invoke Python from the command prompt is controlled by either 1) the `py launcher` app (recommended) or 2) the PATH environment variable.  Here are some examples on Windows, using the `py launcher`:

Show all installed Python versions, with an asterisk next to the default version.

```shell
> py -0p
Installed Pythons found by C:\WINDOWS\py.exe Launcher for Windows
 -3.10-64       C:\Users\jason\AppData\Local\Programs\Python\Python310\python.exe *
 -3.9-64        C:\Users\jason\AppData\Local\Programs\Python\Python39\python.exe
 -3.8-64        C:\Users\jason\AppData\Local\Programs\Python\Python38\python.exe
 -3.7-64        C:\Users\jason\AppData\Local\Programs\Python\Python37\python.exe
```

Invoke default interpreter using `py launcher`.

```shell
> py
Python 3.10.4 (tags/v3.10.4:9d38120, Mar 23 2022, 23:13:41) [MSC v.1929 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

Invoke a different version using `py launcher`.

```shell
> py -3.8
Python 3.8.9 (tags/v3.8.9:a743f81, Apr  6 2021, 14:02:34) [MSC v.1928 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

> **Note**: If you aren't using `py launcher`, you can instead use the Windows PATH environment variable to control which verison of python gets launched from the command prompt.  See *Appendix*.

### Linux

On Linux, typically there are different commands to invoke different versions of Python.

Invoke default Python 3.x version.

```shell
> python3
Python 3.6.8 (default, Sep  9 2021, 07:49:02)
[GCC 8.5.0 20210514 (Red Hat 8.5.0-3)] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

Invoke a specific version of Python.

```shell
> python3.8
Python 3.8.8 (default, Aug 11 2021, 06:52:42)
[GCC 8.5.0 20210514 (Red Hat 8.5.0-3)] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

## Step 3: Create a virtual environment

Working in a virtual environment is not strictly necessary, but highly recommended. It’s a sandbox where you can install Python packages. The sandbox is separated from the main Python install so that if you mess anything up, just simply delete the virtual env folder and create a new one. It also helps in cases where different packages have different, conflicting requirements or dependencies. Creating a virtual env copies over some Python files from the main Python installation folder to your virtual env folder. Open a terminal and navigate to a directory where you would like to place the virtual env folder.  Then use one of the below commands, substituting the correct invocation of the correct Python version that you determined from the previous steps.

On Windows:

```shell
    > py -3.8 -m venv <env_folder_name>
```

On Linux:

```shell
    > python3.8 -m venv <env_folder_name>
```

This will create a virtual environment in a new folder with some subfolders and a cfg file. Common virtual env folder names include names with leading dots such as .venv, .venv38, etc., but the folder name can be anything you prefer. A common practice is to place the virtual env folder inside the project folder for the particular project you are working on.  But, a virtual env folder can be created anywhere.

## Step 4: Activate the virtual environment

On Windows, run the batch file **<env_folder_name>\\Scripts\\activate.bat**, as shown below. Don't run the batch file by double clicking on it because that won't help - it needs to be run in the terminal.

```shell
    C:\Users\jason> .venv38\Scripts\activate
    (.venv38)C:\Users\jason>
```

On Linux, run the activate script from a terminal as shown below.

```shell
    jb [~] $ source .venv38/bin/activate
    (.venv38) jb [~] $
```

On both Windows and Linux, this will change the command prompt to reflect the current virtual env name in parenthesis before the prompt. Now, if you pip install packages, they will only be installed in this `.venv38` virtual environment folder and not the main system-wide Python install. In fact, any Python-related commands will now be executed by the virtual env.

> *Important*: Once you activate a virtual env, you should no longer use the commands discussed previously in the *Running the Correct Python version* step.

Any invocation of Python or Python program (for example, `pip`) should be performed without any qualification of the Python version.  For example, to invoke Python after the virtual env is activated, just type `python` (works on both Windows and Linux).

```shell
    (.venv38)C:\Users\jason> python
```

## Step 5: Install all desired 3rd party libraries

With the virtual environment activated, use pip to install any desired libraries.  They will be downloaded from PYPI.org (unless you have a special repository set up or point to a local path on your machine).

```shell
    (.venv38)> pip install numpy
    (.venv38)> pip install pandas
    (.venv38)> pip install /path/to/local/wheelfile/some_library.whl
```

## Step 6: Install and setup development tools

There are several options for development of Python.  Some choices are outlined below.

### VS Code

A top choice for many folks not only for Python, but for other languages.  Just do a web search and follow Microsoft's download and install instructions for your OS.  It comes pretty bare bones, so after you've installed it, you'll want to install a bunch of Python-related extensions from the Extension Marketplace, like the official Microsoft extensions called "Python", "Pylance", and "Jupyter".

### JupyterLab

A great environment, especially for working with IPython Notebooks (.ipynb files).  To get JupyterLab, just pip install it.

```shell
    (.venv38)> pip install jupyterlab
```

To launch JupyterLab, from a command promt with your virtual environment activated, type ``jupyter-lab``.  This will open JupyterLab in your default web browser.

JupyterLab is similar to VS Code in that it is made to be extended by addons in order to provide specific functionality.  There are many "pre-built" extensions on [**PYPI**](https://pypi.org/search/?q=jupyterlab&o=-created&c=Framework+%3A%3A+Jupyter) that can be installed, as well as "source" extensions on [**npm**](https://www.npmjs.com/search?q=keywords:jupyterlab-extension) that can be installed after an additional build step.  To build an extension, you must first install Node.js from [**here**](https://nodejs.org).  There are many useful extensions, but there is one in particular I will recommend you get: [**Language Server Protocol for JupyterLab**](https://www.npmjs.com/package/@krassowski/jupyterlab-lsp).  Here are the steps:

1. pip install jedi-language-server
2. Install Node.js from [**here**](https://nodejs.org).
3. Launch JupyterLab and go to the Extensions Manager on the left.  Enable the Extension Manager and then search for "lsp". Find the **@krassowski/jupyterlab-lsp** extension and install it (see picture below).  This will require a build process (which is why you had to install Node.js.).  You will know everything is working afterwards if you can hover your mouse over a Python function and press Ctrl to get the function's documentation.  Also, as you are typing Python code in a cell, you can press Tab to get autocomplete suggestions.  This info is being provided by Jedi.

![JupyterLab search for LSP](jupyterlab_lsp.png)

### Spyder

Spyder is a great tool for Python development. One feature that makes it popular is its Variable Explorer, which allows you to quickly view all types of data, including Python built-in types like like strings, ints, floats, complex, tuples, dicts, as well as numpy- and pandas-specific objects like arrays, Series, and DataFrames.  I have found it is best to install Spyder into each virtual env that you use, rather than to try to have a global Spyder install and then pointing to a virtual env's path. Just use pip to install spyder, as shown below (with the virtual env still activated).

```shell
    (.venv38)> pip install spyder
```

Then, to launch Spyder, you can simply type `spyder` in the terminal (with the virtual env still activated).

```shell
    (.venv38)> spyder
```

You can also optionally just navigate to the Scripts folder inside of the virtual env and find spyder.exe and launch it by double clicking it. You can optionally make a shortcut to this executable and pin it on the Windows Start Menu. Take a minute to look over all the executables that reside in the Scripts folder of your virtual env.  When you install additional Python libraries, other executables may be placed at this location.

## Appendix - Using PATH in Windows

When invoking Python from a Windows command prompt, you will want to make sure it starts the correct version of Python.  This can be controlled either by 1) ``py launcher`` or 2) your PATH environment variable.  I find the ``py launcher`` to be the easiest method (see previous sections).  However, if you would prefer the PATH method, you can follow the steps in this Appendix.

### Step 1: Manually add to the PATH

If you allowed the Python installer to add to your PATH variable, you should be all set, and you can skip to the next Step. If not, you will need to manually add to your PATH, so read on...

There are two folder locations that should be added to your PATH for any given Python install: the base install folder and the Scripts folder. Edit your Windows Environment Variables, adding the two paths as shown in the image below. The two paths should be C:\\Users\\UserName\\AppData\\Local\\Programs\\Python\\Python38 and C:\\Users\\UserName\\AppData\\Local\\Programs\\Python\\Python38\\Scripts.

![Windows PATH Environment Variable](windows_path.png)

However, if you installed Python into a non-default location, make the necessary adjustments. Note that the paths to Python 3.8 and its Scripts folder may need to be moved up to the top if you have other installations of Python on your machine.  If you have more than one Python version installed, the version that appears first in the PATH environment variable is the one that will execute Python commands in a terminal.

### Step 2: Check the PATH

There are a couple ways to check the version to which your PATH is pointing.

The first way is to open a command prompt and type the command `where python`. The first Python listed in the output in the terminal should be the python.exe in the **Python38** folder.  If not, the PATH is set up incorrectly, and you should go back to Step 1.

Input:

```shell
    > where python
```

Output:

```shell
    C:\Users\UserName\AppData\Local\Programs\Python\Python38\python.exe
    C:\Users\UserName\AppData\Local\Programs\Python\Python39\python.exe
    C:\Users\UserName\AppData\Local\Microsoft\WindowsApps\python.exe
```

If you are convinced it's set up correctly, you are done.  If you are not sure, or if you are curious, you can proceed to the second way to verify, which involves starting Python.

Input:

```shell
    > python
```

Output:

```shell
    Python 3.8.5 (tags/v3.8.5, Jul 20 2020, 15:57:54) [MSC v.1924 64 bit (AMD64)] on win32
    Type "help", "copyright", "credits" or "license" for more information.
    >>>
```

At the Python command prompt, you can check the location of the Python executable, which should match the location where you installed. When you're done, you can quit Python as shown.

```python
    >>> import sys
    >>> sys.executable
    'C:\\Users\\UserName\\AppData\\Local\\Programs\\Python\\Python38\\python.exe'
    >>> quit()
```
